export interface TypeOfError {
  type: string;
  adjustValueCapability(value: number): number;
}
